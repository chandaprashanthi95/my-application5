package com.example.myapplication5;

public class MyListData {
    private String usename;
    private String password;

    public MyListData() {
    }

    public String getUsename() {
        return usename;
    }

    public void setUsename(String usename) {
        this.usename = usename;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MyListData(String usename, String password) {
        this.usename = usename;
        this.password = password;
    }
}