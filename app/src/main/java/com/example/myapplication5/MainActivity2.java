package com.example.myapplication5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity {
    Context context;
    RecyclerView recyclerView9;
    DatabaseReference databaseReference;
    ArrayList<DashboardModel> datalist2 = new ArrayList<>();
    MyAdapterClass myAdapterClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView9 = findViewById(R.id.recyclerView1);
        databaseReference = FirebaseDatabase.getInstance().getReference();

        DashboardModel dashboardModel = new DashboardModel();

        dashboardModel.setPerson_name1("Prashanthi ");
        dashboardModel.setPerson_contact1("8574758858");
        dashboardModel.setPerson_mail1("chandaprashanthi95@gmail.com");
        dashboardModel.setPerson_address1("Hyderabad Ameerpet");

        DashboardModel dashboardModel1 = new DashboardModel();
        dashboardModel1.setPerson_name1("Venkat Reddy");
        dashboardModel1.setPerson_mail1("chandavenkatReddy@gmail.com");
        dashboardModel1.setPerson_contact1("86756478434");
        dashboardModel1.setPerson_address1("suryapet");

        DashboardModel dashboardModel2 = new DashboardModel();
        dashboardModel2.setPerson_name1("Karishma");
        dashboardModel2.setPerson_mail1("karishmaReddy@gmail.com");
        dashboardModel2.setPerson_contact1("1234567890");
        dashboardModel2.setPerson_address1("Ndcl");
        datalist2.add(dashboardModel);
        datalist2.add(dashboardModel1);
        datalist2.add(dashboardModel2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView9.setLayoutManager(linearLayoutManager);

        // Sending reference and data to Adapter
        MyAdapterClass adapter1 = new MyAdapterClass(datalist2, context);

        // Setting Adapter to RecyclerView
        recyclerView9.setAdapter(adapter1);
        databaseReference.child("Users").setValue(datalist2).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                   // Toast.makeText(MainActivity.this,"OnClicked", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}