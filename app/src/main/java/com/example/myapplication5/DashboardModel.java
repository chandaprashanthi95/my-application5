package com.example.myapplication5;

public class DashboardModel {
    String person_name1;
    String person_contact1;
    String person_mail1;
    String person_address1;
    int img_contact1;
    int img_mail1;
    int img_address1;

    public String getPerson_name1() {
        return person_name1;
    }

    public void setPerson_name1(String person_name1) {
        this.person_name1 = person_name1;
    }

    public String getPerson_contact1() {
        return person_contact1;
    }

    public void setPerson_contact1(String person_contact1) {
        this.person_contact1 = person_contact1;
    }

    public String getPerson_mail1() {
        return person_mail1;
    }

    public void setPerson_mail1(String person_mail1) {
        this.person_mail1 = person_mail1;
    }

    public String getPerson_address1() {
        return person_address1;
    }

    public void setPerson_address1(String person_address1) {
        this.person_address1 = person_address1;
    }

    public int getImg_contact1() {
        return img_contact1;
    }

    public void setImg_contact1(int img_contact1) {
        this.img_contact1 = img_contact1;
    }

    public int getImg_mail1() {
        return img_mail1;
    }

    public void setImg_mail1(int img_mail1) {
        this.img_mail1 = img_mail1;
    }

    public int getImg_address1() {
        return img_address1;
    }

    public void setImg_address1(int img_address1) {
        this.img_address1 = img_address1;
    }

}
