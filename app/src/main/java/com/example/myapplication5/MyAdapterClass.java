package com.example.myapplication5;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapterClass extends RecyclerView.Adapter<MyAdapterClass.ViewHolder> {

        ArrayList<DashboardModel> datalist1;
        Context mcontext;


        public MyAdapterClass(ArrayList<DashboardModel> datalist1, Context mcontext) {
            this.datalist1 = datalist1;
            this.mcontext = mcontext;
        }


        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowitem_list, parent, false);

            // Passing view to ViewHolder
            ViewHolder viewHolder = new ViewHolder(view);


            return viewHolder;


        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.txt1.setText((String) datalist1.get(position).getPerson_name1());
            holder.txt2.setText((String) datalist1.get(position).getPerson_contact1());
            holder.txt3.setText((String) datalist1.get(position).getPerson_mail1());
            holder.txt4.setText((String) datalist1.get(position).getPerson_address1());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(mcontext, "onClicked", Toast.LENGTH_SHORT).show();

                }
            });
        }


        @Override
        public int getItemCount() {
            return datalist1.size();

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView txt1,txt2,txt3,txt4;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                txt1 = (TextView) itemView.findViewById(R.id.name);
                txt2 = (TextView) itemView.findViewById(R.id.age);
                txt3 = (TextView) itemView.findViewById(R.id.add1);
                txt4 = (TextView) itemView.findViewById(R.id.contact);

            }
        }


    }
