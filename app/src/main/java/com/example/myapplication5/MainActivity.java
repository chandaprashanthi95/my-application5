package com.example.myapplication5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {
Button btnInset,btnView;
EditText username,password;
DatabaseReference databaseReference;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        btnInset=findViewById(R.id.btn_log_in);
        btnView=findViewById(R.id.viewbtn);
        username=findViewById(R.id.et_mobile_number);
       password=findViewById(R.id.et_passcode);
       databaseReference= FirebaseDatabase.getInstance().getReference();
       btnInset.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               InsertData();
           }

       });
       btnView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
             //  startActivity((new Intent(MainActivity.this,MainActivity2.class)));
               finish();
           }
       });
    }

    private void InsertData() {
        String usename=username.getText().toString();
        String add=password.getText().toString();
        String id=databaseReference.push().getKey();
        MyListData myListData=new MyListData(usename,add);
        databaseReference.child("Users").child(id).setValue(myListData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    startActivity((new Intent(MainActivity.this,MainActivity2.class)));

                   // Toast.makeText(MainActivity.this,"OnClicked", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}